# Kat's Dotfiles

### Note: not everything will work out of the box, you will need to tweak paths, install dependencies, etc. This is more a collection of various resources than it is a ready out of the box configuration.

## Fonts

All fonts are [undefined medium](https://github.com/andirueckel/undefined-medium). Clone the repo, copy them to `~/.fonts` and run `fc-cache`

## Wallpapers

Animated wallpapers are from [here](https://imgur.com/gallery/0Slze).

## DWM

- Grab a fresh copy of DWM 6.2 from [here](https://dl.suckless.org/dwm/dwm-6.2.tar.gz)
- Extract it
- Copy my `config.h`
- Apply all the patches from patches/ with `patch -p1 < <patch_name.diff>`
- Compile and enjoy

## Alacritty

Copy `alacritty.yml` to `~/.config/alacritty/`.

## Scripts

Two scripts are included.
- wallbulb.py - This sets my TP-Link lightbulb to the dominant wal color. You probably won'
t use this.
- autorun.sh - This script runs on startup and is the main meat of the rice. Open it in your favourite editor and hack it into something that fits your needs. If something is not well commented enough or you're having difficulty, feel free to message me on [Telegram](https://t.me/aliceinuserland) 
