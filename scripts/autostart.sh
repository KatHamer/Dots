#!/bin/bash
# DWM autostart
# Called by DWM to run stuff at startup, also does bar stuff
# By Katelyn Hamer
# To anybody reading this code in future, I'm terribly sorry

KEYMAP="gb"
BULB_IP="192.168.1.247"  # IP of bulb to sync color scheme with

# Mem: format output of free to give RAM usage stats
mem() {
	local used=$(free -h | sed -n 2p | awk '{ print $3 }')  # Second line, third column of free = total mem
	echo "$used"
}

# Get currently playing song 
now_playing() {
	if ! pgrep -x cmus &> /dev/null; then
		echo "not playing"
	else
                local remote_data="$(cmus-remote -Q)"
                local artist=$(echo "$remote_data" | grep "albumartist" | cut -f 3- -d " ")
                local title=$(echo "$remote_data" | grep "title" | cut -f 3- -d " ")
		echo "(cmus) $artist - $title"
	fi
}

# Get current time
clock() {
	date "+%H:%M"
}

# Set a random animated wallpaper, and coordinate lights and terminal scheme with it
fancy_wall() {
	local directory=$1
	local directory_listing="$(ls "$directory")"
	local choice=$(echo "$directory_listing" | shuf -n 1)
	echo "Using wallpaper $directory/$choice..."
	xwinwrap -ov -ni -fs -d -- mpv -wid WID --keepaspect=no --loop "$directory/$choice"  # Set the animated wallpaper
	wal -i "$directory/$choice" -n  # Generate a color scheme
	python "$HOME/.dwm/walbulb.py" $BULB_IP  # Set the bulb to match the primary color
}

# Set wallpaper and keymap
fancy_wall "$HOME/Media/Wallpapers/Animated/"
setxkbmap $KEYMAP

# Loop to update bar
while true; do
	barstring="$(mem) | $(now_playing) | $(clock)"
	xsetroot -name " $barstring"
	sleep 1
done & 
